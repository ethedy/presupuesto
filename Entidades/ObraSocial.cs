﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
  public class ObraSocial
  {
    public int ID { get; set; }

    public string Nombre { get; set; }

    public bool Activo { get; set; }
  }
}
